/*
 * Copyright (C) 2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Gilapic.
 *
 * Gilapic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.gitlabapic.v3;

import java.text.Collator;
import java.util.Comparator;

/**
 *
 * @author TIP
 *
 */
public class IssueComparator implements Comparator<Issue>
{

    public enum Sorting
    {
        ID,
        IID,
        TITLE
    }

    private Sorting sorting;

    /**
     *
     * @param sorting
     */
    public IssueComparator(final Sorting sorting)
    {
        this.sorting = sorting;
    }

    /**
     * 
     */
    @Override
    public int compare(final Issue alpha, final Issue bravo)
    {
        int result;

        result = compare(alpha, bravo, this.sorting);

        //
        return result;
    }

    /**
     *
     * @param alpha
     * @param bravo
     * @return
     */
    public static int compare(final Integer alpha, final Integer bravo)
    {
        int result;

        //
        if ((alpha == null) && (bravo == null))
        {
            //
            result = 0;

        }
        else if (alpha == null)
        {
            //
            result = -1;

        }
        else if (bravo == null)
        {
            //
            result = +1;

        }
        else
        {
            //
            result = alpha.compareTo(bravo);
        }

        //
        return result;
    }

    /**
     * 
     */
    public static int compare(final Issue alpha, final Issue bravo, final Sorting sorting)
    {
        int result;

        //
        switch (sorting)
        {
            case ID:
            default:
                result = compare(getId(alpha), getId(bravo));
            break;

            case IID:
                result = compare(getIid(alpha), getIid(bravo));
            break;

            case TITLE:
                //
                result = compare(getTitle(alpha), getTitle(bravo));
            break;
        }

        //
        return result;
    }

    /**
     *
     * @param alpha
     * @param bravo
     * @return
     */
    public static int compare(final String alpha, final String bravo)
    {
        int result;

        //
        if ((alpha == null) && (bravo == null))
        {
            //
            result = 0;

        }
        else if (alpha == null)
        {
            //
            result = -1;

        }
        else if (bravo == null)
        {
            //
            result = +1;

        }
        else
        {
            //
            result = Collator.getInstance().compare(alpha, bravo);
        }

        //
        return result;
    }

    /**
     *
     * @param source
     * @return
     */
    public static Integer getId(final Issue source)
    {
        Integer result;

        //
        if (source == null)
        {
            //
            result = null;
        }
        else
        {
            //
            result = source.getId();
        }

        //
        return result;
    }

    /**
     *
     * @param source
     * @return
     */
    public static Integer getIid(final Issue source)
    {
        Integer result;

        //
        if (source == null)
        {
            //
            result = null;
        }
        else
        {
            //
            result = source.getIid();
        }

        //
        return result;
    }

    /**
     *
     * @param source
     * @return
     */
    public static String getTitle(final Issue source)
    {
        String result;

        //
        if (source == null)
        {
            //
            result = null;
        }
        else
        {
            //
            result = source.getTitle();
        }

        //
        return result;
    }
}
