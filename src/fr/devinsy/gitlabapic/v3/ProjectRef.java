/*
 * Copyright (C) 2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Gilapic.
 *
 * Gilapic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.gitlabapic.v3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class ProjectRef
{
    private static final Logger logger = LoggerFactory.getLogger(ProjectRef.class);

    private String website;
    private int id;
    private String privateToken;

    /**
     *
     * @param website
     * @param id
     * @param privateToken
     */
    public ProjectRef(final String website, final int id, final String privateToken)
    {
        this.website = website;
        this.id = id;
        this.privateToken = privateToken;
    }

    public int getId()
    {
        return this.id;
    }

    public String getPrivateToken()
    {
        return this.privateToken;
    }

    public String getWebsite()
    {
        return this.website;
    }

    public void setId(final int id)
    {
        this.id = id;
    }

    public void setPrivateToken(final String privateToken)
    {
        this.privateToken = privateToken;
    }

    public void setWebsite(final String website)
    {
        this.website = website;
    }

    /**
     *
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("[website=%s][id=%d][privateToken=%s]", this.website, this.id, this.privateToken);

        //
        return result;
    }
}
