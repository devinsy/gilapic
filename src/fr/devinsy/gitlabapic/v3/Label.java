/*
 * Copyright (C) 2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Gilapic.
 *
 * Gilapic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.gitlabapic.v3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Label
{
    private static final Logger logger = LoggerFactory.getLogger(Label.class);

    private String name;
    private String color;
    private String description;

    /**
     *
     * @param name
     * @param color
     */
    public Label()
    {
        this(null, null, null);
    }

    /**
     *
     * @param name
     * @param color
     */
    public Label(final String name, final String color)
    {
        this(name, color, null);
    }

    /**
     *
     * @param name
     * @param color
     * @param description
     */
    public Label(final String name, final String color, final String description)
    {
        this.name = name;
        this.color = color;
        this.description = description;
    }

    public String getColor()
    {
        return this.color;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getName()
    {
        return this.name;
    }

    public void setColor(final String color)
    {
        this.color = color;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    /**
     *
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("[name=%s][color=%s][description=%s]", this.name, this.color, this.description);

        //
        return result;
    }
}
