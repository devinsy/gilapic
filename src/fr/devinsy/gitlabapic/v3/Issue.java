/*
 * Copyright (C) 2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Gilapic.
 *
 * Gilapic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.gitlabapic.v3;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.strings.StringList;

/**
 *
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Issue
{
    private static final Logger logger = LoggerFactory.getLogger(Issue.class);

    private int id;
    private int iid;
    private int projectId;
    private String title;
    private String description;
    private String state;
    private String createdAt;
    private String updatedAt;
    private StringList labels;
    private String milestone;
    private String assignee;
    private int authorId;
    private String authorName;
    private String authorUserName;
    private boolean subscribed;

    /**
     *
     */
    public Issue()
    {
        this.labels = new StringList();
    }

    public String getAssignee()
    {
        return this.assignee;
    }

    public int getAuthorId()
    {
        return this.authorId;
    }

    public String getAuthorName()
    {
        return this.authorName;
    }

    public String getAuthorUserName()
    {
        return this.authorUserName;
    }

    public String getCreatedAt()
    {
        return this.createdAt;
    }

    public String getDescription()
    {
        return this.description;
    }

    public int getId()
    {
        return this.id;
    }

    public int getIid()
    {
        return this.iid;
    }

    public String getMilestone()
    {
        return this.milestone;
    }

    public int getProjectId()
    {
        return this.projectId;
    }

    public String getState()
    {
        return this.state;
    }

    public String getTitle()
    {
        return this.title;
    }

    public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    public boolean isSubscribed()
    {
        return this.subscribed;
    }

    public boolean isSuscribed()
    {
        return this.subscribed;
    }

    public StringList labels()
    {
        return this.labels;
    }

    public void setAssignee(final String assignee)
    {
        this.assignee = assignee;
    }

    public void setAuthorId(final int authorId)
    {
        this.authorId = authorId;
    }

    public void setAuthorName(final String authorName)
    {
        this.authorName = authorName;
    }

    public void setAuthorUserName(final String authorUserName)
    {
        this.authorUserName = authorUserName;
    }

    public void setCreatedAt(final String createdAt)
    {
        this.createdAt = createdAt;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public void setId(final int id)
    {
        this.id = id;
    }

    public void setIid(final int iid)
    {
        this.iid = iid;
    }

    public void setMilestone(final String milestone)
    {
        this.milestone = milestone;
    }

    public void setProjectId(final int projectId)
    {
        this.projectId = projectId;
    }

    public void setState(final String state)
    {
        this.state = state;
    }

    public void setSubscribed(final boolean subscribed)
    {
        this.subscribed = subscribed;
    }

    public void setTitle(final String title)
    {
        this.title = title;
    }

    public void setUpdatedAt(final String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    /**
     *
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("[id=%d][iid=%d][projectId=%d][title=%s][description=%s][state=%s][createdAt=%s][updatedAt=%s][authorId=%s][authorUserName=%s][subscribed=%b]", this.id, this.iid,
                this.projectId, this.title, StringUtils.abbreviate(this.description, 20), this.state, this.createdAt, this.updatedAt, this.authorId, this.authorUserName, this.subscribed);

        //
        return result;
    }
}
