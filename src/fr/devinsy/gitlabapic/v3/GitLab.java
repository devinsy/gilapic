/*
 * Copyright (C) 2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Gilapic.
 *
 * Gilapic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.gitlabapic.v3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.gilapic.GitLabAPIException;
import fr.devinsy.gilapic.Utils;
import fr.devinsy.util.strings.StringList;

/**
 *
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GitLab
{
    private static final Logger logger = LoggerFactory.getLogger(GitLab.class);

    public static final String DEFAULT_CHARSET_NAME = "UTF-8";
    public static final int NOID = -1;

    public static void closeIssue(final ProjectRef source, final int issueId, final String updatedAt) throws GitLabAPIException
    {
        editIssue(source, issueId, null, null, null, null, null, "close", updatedAt);
    }

    /**
     *
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static void copyLabels(final ProjectRef source, final ProjectRef target) throws GitLabAPIException
    {
        Labels labels = GitLab.searchLabels(source);
        for (Label label : labels)
        {
            GitLab.createLabel(target, label);
        }
    }

    /**
     *
     * @param args
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static int createIssue(final ProjectRef project, final Issue issue) throws GitLabAPIException
    {
        int result;

        result = createIssue(project, issue.getTitle(), issue.getDescription(), issue.getAssignee(), issue.getMilestone(), issue.labels(), issue.getCreatedAt());

        //
        return result;
    }

    /**
     *
     * CAUTION: created_at parameter is ignored in case of non project owner or
     * admin.
     *
     * @param args
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static int createIssue(final ProjectRef project, final String title, final String description, final String assigneeId, final String milestoneId, final StringList labels,
            final String createdAt) throws GitLabAPIException
    {
        int result;

        try
        {
            String url = String.format("%sapi/v3/projects/%d/issues", project.getWebsite(), project.getId());

            StringList fixedLabels;
            if (labels == null)
            {
                fixedLabels = new StringList();
            }
            else
            {
                fixedLabels = labels;
            }

            //
            String response = Utils.httpPost(new URI(url), project.getPrivateToken(), "title", fixBug19902(title), "description", fixBug19902(description), "labels", fixedLabels.toStringWithCommas(),
                    "created_at", createdAt);

            //
            JsonReader source = Json.createReader(new StringReader(response));
            JsonObject item = source.readObject();

            result = item.getInt("id");
        }
        catch (IOException | URISyntaxException | HttpException exception)
        {
            logger.error(exception.getMessage(), exception);
            throw new GitLabAPIException("Error creating issue: " + exception.getMessage(), exception);
        }

        //
        return result;
    }

    /**
     *
     * @param args
     * @throws IOException
     * @throws URISyntaxException
     * @throws GitLabAPIException
     */
    public static String createLabel(final ProjectRef project, final Label label) throws GitLabAPIException
    {
        String result;

        result = createLabel(project, label.getName(), label.getColor(), label.getDescription());

        //
        return result;
    }

    /**
     *
     * @param args
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static String createLabel(final ProjectRef project, final String name, final String color, final String description) throws GitLabAPIException
    {
        String result;

        String url = String.format("%sapi/v3/projects/%d/labels", project.getWebsite(), project.getId());

        try
        {
            String response = Utils.httpPost(new URI(url), project.getPrivateToken(), "name", name, "color", color, "description", description);

            JsonReader source = Json.createReader(new StringReader(response));
            JsonObject item = source.readObject();

            result = item.getString("name");
        }
        catch (IOException | URISyntaxException | HttpException exception)
        {
            logger.error(exception.getMessage(), exception);
            throw new GitLabAPIException("Error creating label: " + exception.getMessage(), exception);
        }

        //
        return result;
    }

    /**
     *
     * CAUTION: created_at parameter is ignored in case of non project owner or
     * admin.
     *
     * @param args
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static int createNote(final ProjectRef project, final int parentId, final String body, final String createdAt) throws GitLabAPIException
    {
        int result;
        try
        {
            String url = String.format("%sapi/v3/projects/%d/issues/%d/notes", project.getWebsite(), project.getId(), parentId);

            String response = Utils.httpPost(new URI(url), project.getPrivateToken(), "body", fixBug19902(body), "created_at", createdAt, "system", "true");

            logger.debug("response=[{}]", response);

            result = 0;
        }
        catch (IOException | URISyntaxException | HttpException exception)
        {
            logger.error(exception.getMessage(), exception);
            throw new GitLabAPIException("Error creating note: " + exception.getMessage(), exception);
        }

        //
        return result;
    }

    /**
     *
     * CAUTION: created_at parameter is ignored in case of non project owner or
     * admin.
     *
     * @param args
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static int editIssue(final ProjectRef project, final int issueId, final String title, final String description, final String assigneeId, final String milestoneId, final StringList labels,
            final String stateEvent, final String updatedAt) throws GitLabAPIException
    {
        int result;

        try
        {
            String url = String.format("%sapi/v3/projects/%d/issues/%d", project.getWebsite(), project.getId(), issueId);

            StringList fixedLabels;
            if (labels == null)
            {
                fixedLabels = new StringList();
            }
            else
            {
                fixedLabels = labels;
            }

            //
            String response = Utils.httpPut(new URI(url), project.getPrivateToken(), "title", fixBug19902(title), "description", fixBug19902(description), "labels", fixedLabels.toStringWithCommas(),
                    "state_event", stateEvent, "updated_at", updatedAt);

            //
            JsonReader source = Json.createReader(new StringReader(response));
            JsonObject item = source.readObject();

            result = item.getInt("id");
        }
        catch (IOException | URISyntaxException | HttpException exception)
        {
            logger.error(exception.getMessage(), exception);
            throw new GitLabAPIException("Error creating issue: " + exception.getMessage(), exception);
        }

        //
        return result;
    }

    /**
     *
     * https://gitlab.com/gitlab-org/gitlab-ce/issues/19902
     *
     * @param source
     * @return
     */
    private static String fixBug19902(final String string)
    {
        String result;

        if (string == null)
        {
            result = null;
        }
        else
        {
            result = string.replace("é", "e").replace("ç", "c").replace("è", "e");
        }

        //
        return result;
    }

    /**
     *
     * @param sourceWebsite
     * @param projectId
     * @param privateToken
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static void importLabels(final ProjectRef source, final ProjectRef target) throws GitLabAPIException
    {
        Labels labels = GitLab.searchLabels(source);

        for (Label label : labels)
        {
            createLabel(target, label);
        }
    }

    /**
     *
     * @param args
     * @throws IOException
     * @throws GitLabAPIException
     * @throws URISyntaxException
     */
    public static Issue searchIssue(final ProjectRef project, final int issueId) throws GitLabAPIException
    {
        Issue result;

        try
        {
            logger.debug("[project={}][issueId={}]", project, issueId);

            String url = String.format("%sapi/v3/projects/%d/issues/%d", project.getWebsite(), project.getId(), issueId);

            logger.debug("url=[{}]", url);

            //
            byte[] data = Utils.httpGet(new URL(url), project.getPrivateToken());

            //
            StringList json = new StringList(IOUtils.readLines(new ByteArrayInputStream(data), DEFAULT_CHARSET_NAME));
            logger.debug("json={}", json);

            //
            JsonReader source = Json.createReader(new ByteArrayInputStream(data));

            JsonArray items = source.readArray();

            result = new Issue();
            /*
            result.setId(item.getInt("id"));
            result.setIid(item.getInt("iid"));
            result.setProjectId(item.getInt("project_id"));
            result.setTitle(item.getString("title"));
            result.setDescription(item.getString("description"));
            result.setState(item.getString("state"));
            result.setCreatedAt(item.getString("created_at"));
            result.setUpdatedAt(item.getString("updated_at"));
            // private StringList labels;
            // issue.setMilestone(item.getString("milestone"));
            // private String assignee;
            result.setAuthorId(item.getJsonObject("author").getInt("id"));
            result.setAuthorUserName(item.getJsonObject("author").getString("username"));
            result.setSubscribed(item.getBoolean("subscribed"));
            */

        }
        catch (IOException exception)
        {
            logger.error(exception.getMessage(), exception);
            throw new GitLabAPIException("Error searching issue: " + exception.getMessage(), exception);
        }

        //
        return result;
    }

    /**
     *
     * @param args
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static Notes searchIssueNotes(final ProjectRef project, final int issueId) throws GitLabAPIException
    {
        Notes result;

        try
        {
            String url = String.format("%sapi/v3/projects/%d/issues/%d/notes", project.getWebsite(), project.getId(), issueId);

            //
            byte[] data = Utils.httpGet(new URL(url), project.getPrivateToken());

            //
            StringList json = new StringList(IOUtils.readLines(new ByteArrayInputStream(data), DEFAULT_CHARSET_NAME));
            logger.debug("json={}", json);

            //
            JsonReader source = Json.createReader(new ByteArrayInputStream(data));

            JsonArray items = source.readArray();

            result = new Notes();
            for (JsonObject item : items.getValuesAs(JsonObject.class))
            {
                Note note = new Note();

                note.setId(item.getInt("id"));
                note.setBody(item.getString("body"));
                note.setCreatedAt(item.getString("created_at"));
                note.setAuthorId(item.getJsonObject("author").getInt("id"));
                note.setAuthorName(item.getJsonObject("author").getString("name"));
                note.setAuthorUserName(item.getJsonObject("author").getString("username"));

                result.add(note);
            }
        }
        catch (IOException exception)
        {
            logger.error(exception.getMessage(), exception);
            throw new GitLabAPIException("Error searching labels: " + exception.getMessage(), exception);
        }

        //
        return result;
    }

    /**
     *
     * @param args
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static Issues searchIssues(final ProjectRef project) throws GitLabAPIException
    {
        Issues result;

        try
        {
            String url = String.format("%sapi/v3/projects/%d/issues", project.getWebsite(), project.getId());

            //
            byte[] data = Utils.httpGet(new URL(url), project.getPrivateToken());

            //
            StringList json = new StringList(IOUtils.readLines(new ByteArrayInputStream(data), DEFAULT_CHARSET_NAME));
            logger.debug("json={}", json);

            //
            JsonReader source = Json.createReader(new ByteArrayInputStream(data));

            JsonArray items = source.readArray();

            result = new Issues();
            for (JsonObject item : items.getValuesAs(JsonObject.class))
            {
                Issue issue = new Issue();

                issue.setId(item.getInt("id"));
                issue.setIid(item.getInt("iid"));
                issue.setProjectId(item.getInt("project_id"));
                issue.setTitle(item.getString("title"));
                issue.setDescription(item.getString("description"));
                issue.setState(item.getString("state"));
                issue.setCreatedAt(item.getString("created_at"));
                issue.setUpdatedAt(item.getString("updated_at"));

                for (JsonString subItem : item.getJsonArray("labels").getValuesAs(JsonString.class))
                {
                    issue.labels().add(subItem.getString());
                }
                // issue.setMilestone(item.getString("milestone"));
                // private String assignee;
                issue.setAuthorId(item.getJsonObject("author").getInt("id"));
                issue.setAuthorName(item.getJsonObject("author").getString("name"));
                issue.setAuthorUserName(item.getJsonObject("author").getString("username"));
                issue.setSubscribed(item.getBoolean("subscribed"));

                result.add(issue);
            }
        }
        catch (IOException exception)
        {
            logger.error(exception.getMessage(), exception);
            throw new GitLabAPIException("Error searching labels: " + exception.getMessage(), exception);
        }

        //
        return result;
    }

    /**
     *
     * @param args
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static Labels searchLabels(final ProjectRef project) throws GitLabAPIException
    {
        Labels result;

        try
        {
            String url = String.format("%sapi/v3/projects/%d/labels", project.getWebsite(), project.getId());

            //
            byte[] data = Utils.httpGet(new URL(url), project.getPrivateToken());

            //
            StringList json = new StringList(IOUtils.readLines(new ByteArrayInputStream(data), DEFAULT_CHARSET_NAME));
            logger.debug("json={}", json);

            //
            JsonReader source = Json.createReader(new ByteArrayInputStream(data));

            JsonArray items = source.readArray();

            result = new Labels();
            for (JsonObject item : items.getValuesAs(JsonObject.class))
            {
                Label label = new Label();

                label.setName(item.getString("name"));
                label.setColor(item.getString("color"));

                result.add(label);
            }
        }
        catch (IOException exception)
        {
            logger.error(exception.getMessage(), exception);
            throw new GitLabAPIException("Error searching labels: " + exception.getMessage(), exception);
        }

        //
        return result;
    }

    /**
     *
     * @param website
     * @param projectName
     *            name of project with namespace
     * @param privateToken
     * @return
     * @throws GitLabAPIException
     * @throws IOException
     * @throws URISyntaxException
     */
    public static ProjectRef searchProject(final String website, final String token, final String privateToken) throws GitLabAPIException
    {
        ProjectRef result;

        try
        {
            String url = String.format("%sapi/v3/projects", website);

            //
            byte[] data = Utils.httpGet(new URL(url), privateToken);

            //
            StringList json = new StringList(IOUtils.readLines(new ByteArrayInputStream(data), DEFAULT_CHARSET_NAME));
            logger.debug("json={}", json);

            //
            JsonReader source = Json.createReader(new ByteArrayInputStream(data));
            JsonArray items = source.readArray();

            //
            boolean ended = false;
            int itemIndex = 0;
            JsonObject searchedItem = null;
            while (!ended)
            {
                if (itemIndex < items.size())
                {
                    JsonObject item = items.getJsonObject(itemIndex);

                    if (StringUtils.equals(item.getString("name_with_namespace"), token))
                    {
                        ended = true;
                        searchedItem = item;
                    }
                    else
                    {
                        itemIndex += 1;
                    }
                }
                else
                {
                    ended = true;
                    searchedItem = null;
                }
            }

            //
            if (searchedItem == null)
            {
                result = null;
            }
            else
            {
                result = new ProjectRef(website, searchedItem.getInt("id"), privateToken);
            }
        }
        catch (IOException exception)
        {
            logger.error(exception.getMessage(), exception);
            throw new GitLabAPIException("Error searching labels: " + exception.getMessage(), exception);
        }

        //
        return result;
    }
}
