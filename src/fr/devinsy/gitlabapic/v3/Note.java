/*
 * Copyright (C) 2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Gilapic.
 *
 * Gilapic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.gitlabapic.v3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Note
{
    private static final Logger logger = LoggerFactory.getLogger(Note.class);

    private int id;
    private String body;
    // "attachment": null,
    private int authorId;
    private String authorName;
    private String authorUserName;
    // "author": {"id": 1, "username": "pipin", "email": "admin@example.com",
    // "name": "Pip", "state": "active", "created_at": "2013-09-30T13:46:01Z" },
    private String createdAt;
    private String updatedAt;
    private boolean system;
    private boolean upvote;
    private boolean downvote;
    private int noteableId;
    private String noteableType;

    /**
     *
     */
    public Note()
    {
    }

    public int getAuthorId()
    {
        return this.authorId;
    }

    public String getAuthorName()
    {
        return this.authorName;
    }

    public String getAuthorUserName()
    {
        return this.authorUserName;
    }

    public String getBody()
    {
        return this.body;
    }

    public String getCreatedAt()
    {
        return this.createdAt;
    }

    public int getId()
    {
        return this.id;
    }

    public int getNoteableId()
    {
        return this.noteableId;
    }

    public String getNoteableType()
    {
        return this.noteableType;
    }

    public String getUpdatedAt()
    {
        return this.updatedAt;
    }

    public boolean isDownvote()
    {
        return this.downvote;
    }

    public boolean isSystem()
    {
        return this.system;
    }

    public boolean isUpvote()
    {
        return this.upvote;
    }

    public void setAuthorId(final int authorId)
    {
        this.authorId = authorId;
    }

    public void setAuthorName(final String authorName)
    {
        this.authorName = authorName;
    }

    public void setAuthorUserName(final String authorUserName)
    {
        this.authorUserName = authorUserName;
    }

    public void setBody(final String body)
    {
        this.body = body;
    }

    public void setCreatedAt(final String createdAt)
    {
        this.createdAt = createdAt;
    }

    public void setDownvote(final boolean downvote)
    {
        this.downvote = downvote;
    }

    public void setId(final int id)
    {
        this.id = id;
    }

    public void setNoteableId(final int noteableId)
    {
        this.noteableId = noteableId;
    }

    public void setNoteableType(final String noteableType)
    {
        this.noteableType = noteableType;
    }

    public void setSystem(final boolean system)
    {
        this.system = system;
    }

    public void setUpdatedAt(final String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public void setUpvote(final boolean upvote)
    {
        this.upvote = upvote;
    }

    /**
     *
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("[id=%d][body=%s]", this.id, this.body);

        //
        return result;
    }
}
