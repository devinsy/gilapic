/*
 * Copyright (C) 2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Gilapic.
 *
 * Gilapic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.gitlabapic.v3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Project
{
    private static final Logger logger = LoggerFactory.getLogger(Project.class);

    private int id;
    private String description;
    private String defaultBranch;
    private boolean publicStatus;
    private int visibilityLevel;
    // "ssh_url_to_repo": "git@example.com:diaspora/diaspora-client.git",
    // "http_url_to_repo": "http://example.com/diaspora/diaspora-client.git",
    // "web_url": "http://example.com/diaspora/diaspora-client",
    // "tag_list": [ "example", "disapora client" ],
    // "owner": { "id": 3, "name": "Diaspora", "created_at":
    // "2013-09-30T13: 46: 02Z" },
    private String name;
    private String nameWithNamespace;
    private String path;
    private String pathWithNamespace;
    private boolean issuesEnabled;
    private int openIssuescount;
    // "merge_requests_enabled": true,
    // "builds_enabled": true,
    // "wiki_enabled": true,
    // "snippets_enabled": false,
    private String createdAt;
    // "last_activity_at": "2013-09-30T13: 46: 02Z",
    private int creatorId;
    // "namespace": { "created_at": "2013-09-30T13: 46: 02Z", "description": "",
    // "id": 3, "name": "Diaspora", "owner_id": 1, "path": "diaspora",
    // "updated_at": "2013-09-30T13: 46: 02Z" },
    private boolean archived;
    private String avatarUrl;
    // "shared_runners_enabled": true,
    private int forkCount;
    private int starCount;

    // "runners_token": "b8547b1dc37721d05889db52fa2f02",
    // "public_builds": true

    public String getAvatarUrl()
    {
        return this.avatarUrl;
    }

    public String getCreatedAt()
    {
        return this.createdAt;
    }

    public int getCreatorId()
    {
        return this.creatorId;
    }

    public String getDefaultBranch()
    {
        return this.defaultBranch;
    }

    public String getDescription()
    {
        return this.description;
    }

    public int getForkCount()
    {
        return this.forkCount;
    }

    public int getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getNameWithNamespace()
    {
        return this.nameWithNamespace;
    }

    public int getOpenIssuescount()
    {
        return this.openIssuescount;
    }

    public String getPath()
    {
        return this.path;
    }

    public String getPathWithNamespace()
    {
        return this.pathWithNamespace;
    }

    public int getStarCount()
    {
        return this.starCount;
    }

    public int getVisibilityLevel()
    {
        return this.visibilityLevel;
    }

    public boolean isArchived()
    {
        return this.archived;
    }

    public boolean isIssuesEnabled()
    {
        return this.issuesEnabled;
    }

    public boolean isPublicStatus()
    {
        return this.publicStatus;
    }

    public void setArchived(final boolean archived)
    {
        this.archived = archived;
    }

    public void setAvatarUrl(final String avatarUrl)
    {
        this.avatarUrl = avatarUrl;
    }

    public void setCreatedAt(final String createdAt)
    {
        this.createdAt = createdAt;
    }

    public void setCreatorId(final int creatorId)
    {
        this.creatorId = creatorId;
    }

    public void setDefaultBranch(final String defaultBranch)
    {
        this.defaultBranch = defaultBranch;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public void setForkCount(final int forkCount)
    {
        this.forkCount = forkCount;
    }

    public void setId(final int id)
    {
        this.id = id;
    }

    public void setIssuesEnabled(final boolean issuesEnabled)
    {
        this.issuesEnabled = issuesEnabled;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public void setNameWithNamespace(final String nameWithNamespace)
    {
        this.nameWithNamespace = nameWithNamespace;
    }

    public void setOpenIssuescount(final int openIssuescount)
    {
        this.openIssuescount = openIssuescount;
    }

    public void setPath(final String path)
    {
        this.path = path;
    }

    public void setPathWithNamespace(final String pathWithNamespace)
    {
        this.pathWithNamespace = pathWithNamespace;
    }

    public void setPublicStatus(final boolean publicStatus)
    {
        this.publicStatus = publicStatus;
    }

    public void setStarCount(final int starCount)
    {
        this.starCount = starCount;
    }

    public void setVisibilityLevel(final int visibilityLevel)
    {
        this.visibilityLevel = visibilityLevel;
    }

    /**
     *
     */
    @Override
    public String toString()
    {
        String result;

        result = String.format("[id=%d][name=%s]", this.id, this.name);

        //
        return result;
    }
}
