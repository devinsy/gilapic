/*
 * Copyright (C) 2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Gilapic.
 *
 * Gilapic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.gilapic;

/**
 *
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class GitLabAPIException extends Exception
{
    private static final long serialVersionUID = 2320169742273411056L;

    /**
     *
     */
    public GitLabAPIException()
    {
        super();
    }

    /**
     *
     * @param message
     */
    public GitLabAPIException(final String message)
    {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public GitLabAPIException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     *
     * @param cause
     */
    public GitLabAPIException(final Throwable cause)
    {
        super(cause);
    }
}
