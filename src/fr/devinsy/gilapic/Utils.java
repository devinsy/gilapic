/*
 * Copyright (C) 2016 Christian Pierre MOMON <christian.momon@devinsy.fr>
 *
 * This file is part of Gilapic.
 *
 * Gilapic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.devinsy.gilapic;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpException;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.strings.StringList;

/**
 *
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Utils
{
    private static final Logger logger = LoggerFactory.getLogger(Utils.class);

    public static final String DEFAULT_CHARSET_NAME = "UTF-8";

    /**
     *
     * @param source
     * @return
     */
    private static byte[] httpGet(final URL source)
    {
        byte[] result;

        result = httpGet(source, null);

        //
        return result;
    }

    /**
     *
     * @param source
     * @return
     */
    public static byte[] httpGet(final URL source, final String privateToken)
    {
        byte[] result;

        if (source == null)
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else
        {
            URLConnection connection = null;
            try
            {
                // Download file.
                connection = source.openConnection();
                connection.setConnectTimeout(25000);
                connection.setReadTimeout(25000);
                connection.setRequestProperty("charset", "utf-8");

                if (privateToken != null)
                {
                    connection.setRequestProperty("PRIVATE-TOKEN", privateToken);
                }

                result = IOUtils.toByteArray(connection);
            }
            catch (MalformedURLException exception)
            {
                exception.printStackTrace();
                result = null;
            }
            catch (IOException exception)
            {
                exception.printStackTrace();
                result = null;
            }
            finally
            {
                IOUtils.close(connection);
            }
        }

        //
        return result;
    }

    /**
     *
     * @param uri
     * @return
     * @throws IOException
     * @throws HttpException
     */
    public static String httpPost(final URI uri, final String privateToken, final String... parameters) throws IOException, HttpException
    {
        String result;

        logger.debug("[uri={}][privateToken={}][parameters={}]", uri, privateToken, parameters);

        if (uri == null)
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else
        {
            CloseableHttpResponse response = null;
            try
            {
                CloseableHttpClient httpClient = HttpClients.createDefault();
                HttpPost httpPost = new HttpPost(uri);

                if (privateToken != null)
                {
                    httpPost.setHeader("PRIVATE-TOKEN", privateToken);
                }

                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                for (int parameterIndex = 0; parameterIndex < parameters.length; parameterIndex += 2)
                {
                    if (parameters[parameterIndex + 1] != null)
                    {
                        nvps.add(new BasicNameValuePair(parameters[parameterIndex], parameters[parameterIndex + 1]));
                    }
                }

                ByteArrayOutputStream out = new ByteArrayOutputStream(100000);
                new UrlEncodedFormEntity(nvps).writeTo(out);
                logger.debug("urlencoded=[{}]", out.toString());
                httpPost.setEntity(new UrlEncodedFormEntity(nvps));
                logger.debug("httpPost={}", httpPost);

                response = httpClient.execute(httpPost);

                logger.debug("status line={} [{}]", response.getStatusLine(), uri);
                if ((response.getStatusLine().getStatusCode() != 200) && (response.getStatusLine().getStatusCode() != 201))
                {
                    throw new HttpException("Status code returned: " + response.getStatusLine());
                }
                else
                {
                    result = new StringList(IOUtils.readLines(response.getEntity().getContent())).toString();
                }
            }
            finally
            {
                response.close();
            }
        }

        //
        return result;
    }

    /**
     *
     * @param uri
     * @param privateToken
     * @param parameters
     * @return
     * @throws IOException
     * @throws HttpException
     */
    public static String httpPut(final URI uri, final String privateToken, final String... parameters) throws IOException, HttpException
    {
        String result;

        logger.debug("[uri={}][privateToken={}][parameters={}]", uri, privateToken, parameters);

        if (uri == null)
        {
            throw new IllegalArgumentException("Null parameter.");
        }
        else
        {
            CloseableHttpResponse response = null;
            try
            {
                CloseableHttpClient httpClient = HttpClients.createDefault();
                HttpPut httpPut = new HttpPut(uri);

                if (privateToken != null)
                {
                    httpPut.setHeader("PRIVATE-TOKEN", privateToken);
                }

                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                for (int parameterIndex = 0; parameterIndex < parameters.length; parameterIndex += 2)
                {
                    if (StringUtils.isNotBlank(parameters[parameterIndex + 1]))
                    {
                        nvps.add(new BasicNameValuePair(parameters[parameterIndex], parameters[parameterIndex + 1]));
                    }
                }

                ByteArrayOutputStream out = new ByteArrayOutputStream(100000);
                new UrlEncodedFormEntity(nvps).writeTo(out);
                logger.debug("urlencoded=[{}]", out.toString());
                httpPut.setEntity(new UrlEncodedFormEntity(nvps));
                logger.debug("httpPut={}", httpPut);

                response = httpClient.execute(httpPut);

                logger.debug("status line={} [{}]", response.getStatusLine(), uri);
                if ((response.getStatusLine().getStatusCode() != 200) && (response.getStatusLine().getStatusCode() != 201))
                {
                    throw new HttpException("Status code returned: " + response.getStatusLine());
                }
                else
                {
                    result = new StringList(IOUtils.readLines(response.getEntity().getContent())).toString();
                }
            }
            finally
            {
                response.close();
            }
        }

        //
        return result;
    }
}
